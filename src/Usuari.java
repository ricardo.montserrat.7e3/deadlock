public class Usuari
{
    private String nom;

    public Usuari(String nom)
    {
        this.nom = nom;
    }

    public String getNom()
    {
        return nom;
    }

    public void setNom(String nom)
    {
        this.nom = nom;
    }

    public void transferencia(CompteBancari origen, CompteBancari desti, double quantity)
    {
        CompteBancari first = origen;
        CompteBancari second = desti;
        if (first.getId() < second.getId())
        {
            first = desti;
            second = origen;
        }
        synchronized (first)
        {
            synchronized (second)
            {
                first.reintegrar(quantity);
                second.ingressar(quantity);
            }
        }
    }
}
