public class Prova
{
    private static final CompteBancari account1 = new CompteBancari(1, 500);
    private static final CompteBancari account2 = new CompteBancari(2, 250);

    public static void main(String[] args)
    {
        Usuari user1 = new Usuari("Ricardo");
        Usuari user2 = new Usuari("Anna");

        Thread makeOperationsUser = new Thread(() ->
        {
            synchronized (account1)
            {
                try { Thread.sleep(101); }catch (Exception e) { System.out.println(e.toString());}
                synchronized (account2)
                {
                    user1.transferencia(account1, account2, 300);
                    try { Thread.sleep(101); }catch (Exception e) { System.out.println(e.toString());}
                }
            }
        });
        Thread makeOperationsOtherUser = new Thread(() ->
        {
            synchronized (account1)
            {
                try { Thread.sleep(101); }catch (Exception e) { System.out.println(e.toString());}
                synchronized (account2)
                {
                    user2.transferencia(account2, account1, 300);
                    try {Thread.sleep(101);}catch (Exception e) { System.out.println(e.toString());}
                }
            }
        });

        makeOperationsUser.start();
        makeOperationsOtherUser.start();

        try
        {
            makeOperationsUser.join();
            makeOperationsOtherUser.join();
        }
        catch (Exception e) {System.out.println(e.toString());}
        System.out.println("\n" + account1);
        System.out.println(account2);
    }
}
