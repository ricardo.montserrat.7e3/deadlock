public class CompteBancari
{
    private int id;
    private double saldo;

    public CompteBancari(int id, double saldo)
    {
        this.id = id;
        this.saldo = saldo;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public double getSaldo()
    {
        return saldo;
    }

    public void setSaldo(double saldo)
    {
        this.saldo = saldo;
    }

    public void ingressar(double quantity)
    {
        saldo = saldo + quantity;
        System.out.println(this + " ingresated " + quantity);
    }

    public void reintegrar(double quantity)
    {
        try
        {
            if (quantity > saldo) throw new Exception("SaldoInsuficientException");
            else
            {
                saldo = saldo - quantity;
                System.out.println(this + " retired succesfully " + quantity);
            }
        }
        catch (Exception e) { System.out.println(e.toString());}
    }

    @Override
    public String toString()
    {
        return "Bank Account{id:" + id + ", available: " + saldo + '}';
    }
}
